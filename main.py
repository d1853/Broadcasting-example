import numpy as np

A = np.array([
    [56.0, 0.0 , 4.4, 68.0],
    [1.2, 104.0, 52.0, 8.0],
    [1.8, 135.0, 99.0, 0.9]
])

print("All products: ", A)

total_cal_per_product = A.sum(axis=0)

print("Total calories per product: ", total_cal_per_product)

percentages = 100 * A / total_cal_per_product

print("Percentages: ", percentages)